package edu.xiaoleiwang.mymeteo.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import edu.xiaoleiwang.mymeteo.R;
import edu.xiaoleiwang.mymeteo.model.CurrentWeather;
import edu.xiaoleiwang.mymeteo.remote.MeteoDataHelper;

/**
 * Created by xiaoleiwang on 2017/1/14.
 */

public class ListWeatherAdapter extends BaseAdapter {
    private CurrentWeather currentPositionWeather = null;
    private Collection<CurrentWeather> weathers = new ArrayList<>();
    private String unit = "";
    private Context cxt = null;
    public ListWeatherAdapter(Context cxt){
        this.cxt = cxt;
    }
    public void setCurrentPositionWeather(CurrentWeather currentWeather,String unit){
        currentPositionWeather = currentWeather;
        this.unit = unit;
        super.notifyDataSetChanged();
    }
    public void addWeather(CurrentWeather weather,String unit){
        weathers.add(weather);
        this.unit = unit;
        super.notifyDataSetChanged();
    }
    public void setWeatherList(Collection<CurrentWeather> list,String unit){
        weathers.clear();
        weathers.addAll(list);
        this.unit = unit;
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return weathers.size()+1;
    }

    @Override
    public Object getItem(int position) {

        return position == 0? currentPositionWeather:weathers.toArray()[position-1];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    class ViewHolder{
        TextView tvTime;
        TextView tvCity;
        TextView tvTemperature;
        ImageView imageIcon;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            LayoutInflater inflater = ((Activity)cxt).getLayoutInflater();
            convertView = inflater.inflate(R.layout.weather_item, parent, false);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            holder.tvCity = (TextView) convertView.findViewById(R.id.tv_city);
            holder.tvTemperature = (TextView) convertView.findViewById(R.id.tv_temp);
            holder.imageIcon = (ImageView) convertView.findViewById(R.id.image_weather);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        CurrentWeather w = (CurrentWeather) getItem(position);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        String cityName = w == null?"-":w.getName();
        String time = w==null?"--:--":sdf.format(new Date(w.getDt()));
        String temperature = w==null?"-":String.valueOf(w.getMain().getTemp());
        String unitSymb = "";
        switch (unit){
            case "metric":
                unitSymb = "°C";
                break;
            case "imperial":
                unitSymb = "°F";
                break;
            case "kelvin":
                unitSymb ="°K";
                break;
        }
        holder.tvCity.setText(cityName);
        holder.tvTime.setText(time);
        holder.tvTemperature.setText(temperature+unitSymb);
        if(w!=null && w.getWeather().size()>0){
            Picasso.with(cxt).load(MeteoDataHelper.getIconUrl(w.getWeather().get(0).getIcon())).fit().into(holder.imageIcon);
        }
        return convertView;
    }
}
