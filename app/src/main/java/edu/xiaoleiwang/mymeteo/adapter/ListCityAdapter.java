package edu.xiaoleiwang.mymeteo.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.geonames.Toponym;

import java.util.ArrayList;
import java.util.Collection;

import edu.xiaoleiwang.mymeteo.R;

/**
 * Created by xiaoleiwang on 2017/1/15.
 */

public class ListCityAdapter extends BaseAdapter {
    private Context cxt;
    private Collection<Toponym> collection = new ArrayList<>();
    public ListCityAdapter(Context cxt){
        this.cxt = cxt;
    }
    public void setCityList(Collection<Toponym> collection){
        this.collection.clear();
        this.collection.addAll(collection);
        super.notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return collection.size();
    }

    @Override
    public Object getItem(int position) {
        return collection.toArray()[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    class ViewHolder{
        TextView city;
        TextView country;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = ((Activity)cxt).getLayoutInflater().inflate(R.layout.city_item,parent,false);
            holder.city = (TextView) convertView.findViewById(R.id.tv_city);
            holder.country = (TextView) convertView.findViewById(R.id.tv_country);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        Toponym city = (Toponym) getItem(position);
        holder.city.setText(city.getName());
        holder.country.setText(city.getCountryName());
        return convertView;
    }
}
