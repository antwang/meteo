package edu.xiaoleiwang.mymeteo.remote;

import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import edu.xiaoleiwang.mymeteo.model.CurrentWeather;
import edu.xiaoleiwang.mymeteo.model.ForcastWeather;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by xiaoleiwang on 2017/1/10.
 */

public class MeteoDataHelper {
    private static final String apiKey = "&APPID=264a83b2835dc9dd9c2504608fbf1837";
    private static final String currentMeteoByCityName = "http://api.openweathermap.org/data/2.5/weather?q={city_name},{country_code}";
    private static final String currentMeteoByCityID="http://api.openweathermap.org/data/2.5/group?id={cityid}";
    private static final String currentMeteoByCoordinates = "http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}";
    private static final String forecastMeteoByCityID="http://api.openweathermap.org/data/2.5/forecast?id={cityid}";
    private static final String forecastMeteoByCoordinates="http://api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}";
    private static final String iconApi="http://openweathermap.org/img/w/";
    private static OkHttpClient client = null;
    private static MeteoDataHelper instance = null;

    private MeteoDataHelper(){
        client = new OkHttpClient();
    }
    private synchronized static void syncInstance(){
        if(instance==null){
            instance = new MeteoDataHelper();
        }
    }
    public static MeteoDataHelper getInstance() {
        if(instance==null){
            syncInstance();
        }
        return instance;
    }
    public String getCurrentMeteoByCoordinates(double lat,double lon,String unit,String lang){
        String langQuery = "".equals(lang)?"":("&lang="+lang);
        Request request = new Request.Builder()
                .url(currentMeteoByCoordinates.replace("{lat}",String.valueOf(lat))
                        .replace("{lon}",String.valueOf(lon))+"&units="+unit
                        +apiKey+langQuery)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e){
            Log.e("MeteoDataHelper","getCurrentPositionMeteo Error:"+e.getLocalizedMessage());
        } finally {
            if(response != null){
                response.close();
            }
        }
        return "";
    }
    public String getCurrentMeteoByCityName(String cityName,String countryCode,String unit,String lang){
        String langQuery = "".equals(lang)?"":("&lang="+lang);
        Request request = new Request.Builder()
                .url(currentMeteoByCityName.replace("{city_name}",cityName)
                        .replace("{country_code}",countryCode)+"&units="+unit
                        +apiKey+langQuery)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e){
            Log.e("MeteoDataHelper","getCurrentMeteoByCityName Error:"+e.getLocalizedMessage());
        } finally {
            if(response != null){
                response.close();
            }
        }
        return "";
    }

    public String getCurrentMeteoByCity(Collection<String> cityIdList, String unit, String lang){

        if(cityIdList.size()!=0){
            String langQuery = "".equals(lang)?"":("&lang="+lang);

            StringBuilder sb = new StringBuilder();
            for(String cityId:cityIdList){
                sb.append(cityId).append(",");
            }
            String ids = sb.toString();
            Request request = new Request.Builder()
                    .url(currentMeteoByCityID.replace("{cityid}",ids.substring(0,ids.length()-1))+"&units="+unit+apiKey+langQuery)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e){
                Log.e("MeteoDataHelper","getCurrentPositionMeteo Error:"+e.getLocalizedMessage());
            } finally {
                if(response != null){
                    response.close();
                }
            }
        }
        return "";
    }
    public String getForecastMeteoByCoordinates(double lat,double lon,String unit,String lang){
        String langQuery = "".equals(lang)?"":("&lang="+lang);
        Request request = new Request.Builder()
                .url(forecastMeteoByCoordinates.replace("{lat}",String.valueOf(lat))
                        .replace("{lon}",String.valueOf(lon))+"&units="+unit
                        +apiKey+langQuery)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e){
            Log.e("MeteoDataHelper","getCurrentPositionMeteo Error:"+e.getLocalizedMessage());
        } finally {
            if(response != null){
                response.close();
            }
        }
        return "";
    }
    public String getForecastMeteoByCity(String cityId, String unit, String lang){
        String langQuery = "".equals(lang)?"":("&lang="+lang);
        Request request = new Request.Builder()
                .url(forecastMeteoByCityID.replace("{cityid}",cityId)+"&units="+unit+apiKey+langQuery)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e){
            Log.e("MeteoDataHelper","getForecastMeteoByCity Error:"+e.getLocalizedMessage());
        } finally {
            if(response != null){
                response.close();
            }
        }
        return "";
    }
    public static String getIconUrl(String iconCode){
        return iconApi+iconCode+".png";
    }
    public byte[] loadIcon(String iconCode){
        Request request = new Request.Builder().url(iconApi+iconCode+".png")
//                .addHeader("Content-Type", "application/json")
                .build();
        Response response = null;
        byte[] res = null;
        try{
            response = client.newCall(request).execute();
            InputStream is = response.body().byteStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[1024];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();
            res = buffer.toByteArray();
        } catch (IOException e){
            Log.e("MeteoDataHelper","loadIcon Error:"+e.getLocalizedMessage());

        } finally {
            if(response != null){
                response.close();
            }
        }
        return res;
    }
    private String byteArrayToBase64(byte[] byteArray){
        String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encodedImage;
    }
    private byte[] base64ToByteArray(String base64String){
        byte[] decodedString = Base64.decode(base64String, Base64.DEFAULT);
        return decodedString;
    }
}
