package edu.xiaoleiwang.mymeteo;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import edu.xiaoleiwang.mymeteo.adapter.ListWeatherAdapter;
import edu.xiaoleiwang.mymeteo.adapter.MainPagerAdapter;
import edu.xiaoleiwang.mymeteo.model.CurrentWeather;
import edu.xiaoleiwang.mymeteo.model.CurrentWeatherGroup;
import edu.xiaoleiwang.mymeteo.remote.MeteoDataHelper;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_LOCALISATION_PERMISSION = 0x99;
    private static final int ADD_CITY_ACTIVITY_RESULT = 0x1;
    private ViewPager pager = null;
    private MainPagerAdapter pagerAdapter = null;
    private ListView listView;
    private ListWeatherAdapter listWeatherAdapter;
    class MeteoLoader extends Thread{

        @Override
        public void run() {
            SharedPreferences sp = getPreferences(Context.MODE_PRIVATE);
            Set<String> citiesSet = sp.getStringSet("cities", new HashSet<String>());
            final String unit = sp.getString("unit","metric");
            String localeCountryCode = getCurrentLocale().getCountry();
            if(!checkLocationPermissionCheck() && isAppOnForeground(MainActivity.this)){
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_LOCALISATION_PERMISSION);
            }else{
                if(checkLocationPermissionCheck()){
                    Location location = getCurrentLocation();
                    if(location != null){
                        String currentMeteoCoordinates = MeteoDataHelper.getInstance().getCurrentMeteoByCoordinates(location.getLatitude(),location.getLongitude(),unit,localeCountryCode);
                        final CurrentWeather weather = new Gson().fromJson(currentMeteoCoordinates,CurrentWeather.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listWeatherAdapter.setCurrentPositionWeather(weather,unit);
                            }
                        });
                    }
                }
            }
            String currentMeteoCities = MeteoDataHelper.getInstance().getCurrentMeteoByCity(citiesSet,unit,localeCountryCode);
            final Set<CurrentWeather> currentWeatherSet = new LinkedHashSet<>();
            CurrentWeatherGroup meteoCities = new Gson().fromJson(currentMeteoCities,CurrentWeatherGroup.class);
            if(meteoCities != null){
                currentWeatherSet.addAll(meteoCities.getCurrentWeather());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listWeatherAdapter.setWeatherList(currentWeatherSet,unit);
                    }
                });
            }
        }
    }

    private boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,SearchActivity.class);
                startActivityForResult(intent,ADD_CITY_ACTIVITY_RESULT);
            }
        });

//        pagerAdapter = new MainPagerAdapter();
//        pager = (ViewPager) findViewById (R.id.view_pager);
//        pager.setAdapter (pagerAdapter);

        // Create an initial view to display; must be a subclass of FrameLayout.
        //LayoutInflater inflater = getLayoutInflater();
//        FrameLayout v0 = (FrameLayout) inflater.inflate (R.layout.one_of_my_page_layouts, null);
//        pagerAdapter.addView (v0, 0);
        listView = (ListView) findViewById(R.id.listview_weather);
        listWeatherAdapter = new ListWeatherAdapter(this);
        listView.setAdapter(listWeatherAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CurrentWeather weather = (CurrentWeather) listWeatherAdapter.getItem(position);
//                listView.setVisibility(View.INVISIBLE);
            }
        });
        new MeteoLoader().start();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == ADD_CITY_ACTIVITY_RESULT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                final String city = data.getStringExtra("city_name");
                final String countryCode = data.getStringExtra("country_code");
                new Thread(){
                    public void run(){
                        SharedPreferences sp = getPreferences(Context.MODE_PRIVATE);
                        Set<String> citiesSet = sp.getStringSet("cities", new LinkedHashSet<String>());
                        final String unit = sp.getString("unit","metric");
                        String localeCountryCode = getCurrentLocale().getCountry();
                        String json = MeteoDataHelper.getInstance().getCurrentMeteoByCityName(city,countryCode,unit,localeCountryCode);
                        if(!json.trim().equals("")){
                            final CurrentWeather weather = new Gson().fromJson(json,CurrentWeather.class);
                            citiesSet.add(String.valueOf(weather.getId()));
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putStringSet("cities",citiesSet);
                            editor.apply();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    listWeatherAdapter.addWeather(weather,unit);
                                }
                            });
                        }
                    }
                }.start();
            }
        }
    }
    @Override
    public void onStart(){
        super.onStart();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private boolean checkLocationPermissionCheck(){
        return (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }
    private Location getCurrentLocation(){
        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);
        // Getting Current Location
        return locationManager.getLastKnownLocation(provider);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCALISATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new MeteoLoader().start();
                }
            }
            return;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    private Locale getCurrentLocale(){
        Locale current;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            current = getResources().getConfiguration().getLocales().get(0);
        } else{
            //noinspection deprecation
            current = getResources().getConfiguration().locale;
        }
        return current;
    }

}
