package edu.xiaoleiwang.mymeteo;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.geonames.Toponym;
import org.geonames.ToponymSearchCriteria;
import org.geonames.ToponymSearchResult;
import org.geonames.WebService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import edu.xiaoleiwang.mymeteo.adapter.ListCityAdapter;

public class SearchActivity extends AppCompatActivity {
    private ListView listView;
    private ListCityAdapter adapter;
    private Future queryThread = null;
    private ExecutorService executorService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        executorService = Executors.newSingleThreadExecutor();
        listView = (ListView) findViewById(R.id.list_city);
        adapter = new ListCityAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toponym city = (Toponym) adapter.getItem(position);
                Intent intent = getIntent();
                intent.putExtra("city_name", city.getName());
                intent.putExtra("country_code", city.getCountryCode());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        SearchView searchView = (SearchView) findViewById(R.id.edit_query);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(queryThread != null && !queryThread.isDone()){
                    queryThread.cancel(true);
                }
                queryThread = executorService.submit(new QueryWorker(query));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length()>2){
                    if(queryThread != null && !queryThread.isDone()){
                        queryThread.cancel(true);
                    }
                    queryThread = executorService.submit(new QueryWorker(newText));
                }
                return false;
            }
        });
    }
    class QueryWorker implements Callable {
        private String query;
        QueryWorker(String query){
            this.query = query;
        }
        @Override
        public Object call() throws Exception {
            final Collection<Toponym> res = queryCities(query);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.setCityList(res);
                }
            });

            return res;
        }
    }
    private Collection<Toponym> queryCities(String query) throws Exception {
        Collection<Toponym> res = new ArrayList<>();
        WebService.setUserName("wang1128"); // add your username here

        ToponymSearchCriteria searchCriteria = new ToponymSearchCriteria();
        searchCriteria.setQ(query);
        searchCriteria.setMaxRows(5);
        ToponymSearchResult searchResult = WebService.search(searchCriteria);
        for (Toponym toponym : searchResult.getToponyms()) {
            res.add(toponym);
        }
        return res;
    }
}
