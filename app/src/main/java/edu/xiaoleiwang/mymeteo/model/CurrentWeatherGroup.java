
package edu.xiaoleiwang.mymeteo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentWeatherGroup {

    @SerializedName("cnt")
    @Expose
    private long cnt;
    @SerializedName("list")
    @Expose
    private java.util.List<CurrentWeather> currentWeather = null;

    public long getCnt() {
        return cnt;
    }

    public void setCnt(long cnt) {
        this.cnt = cnt;
    }

    public java.util.List<CurrentWeather> getCurrentWeather() {
        return currentWeather;
    }

    public void setCurrentWeather(java.util.List<CurrentWeather> currentWeather) {
        this.currentWeather = currentWeather;
    }

}
