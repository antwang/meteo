
package edu.xiaoleiwang.mymeteo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import edu.xiaoleiwang.mymeteo.model.forecastweathercontents.City;
import edu.xiaoleiwang.mymeteo.model.forecastweathercontents.ListWeather;

public class ForcastWeather {

    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private double message;
    @SerializedName("cnt")
    @Expose
    private long cnt;
    @SerializedName("list")
    @Expose
    private java.util.List<ListWeather> list = null;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public long getCnt() {
        return cnt;
    }

    public void setCnt(long cnt) {
        this.cnt = cnt;
    }

    public java.util.List<ListWeather> getList() {
        return list;
    }

    public void setList(java.util.List<ListWeather> list) {
        this.list = list;
    }

}
