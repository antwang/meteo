package edu.xiaoleiwang.mymeteo;

import android.content.Context;
import android.os.Build;
import android.test.InstrumentationTestCase;
import android.test.mock.MockContext;
import android.util.Log;

import org.geonames.Toponym;
import org.geonames.ToponymSearchCriteria;
import org.geonames.ToponymSearchResult;
import org.geonames.WebService;
import org.junit.Test;

import java.util.Arrays;
import java.util.Locale;

import edu.xiaoleiwang.mymeteo.remote.MeteoDataHelper;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    private Context cxt = new MockContext();
    @Test
    public void testLoadCities() throws Exception {
//        assertEquals(4, 2 + 2);
        WebService.setUserName("wang1128"); // add your username here

        ToponymSearchCriteria searchCriteria = new ToponymSearchCriteria();
        searchCriteria.setQ("paris");
        searchCriteria.setMaxRows(5);
        ToponymSearchResult searchResult = WebService.search(searchCriteria);
        for (Toponym toponym : searchResult.getToponyms()) {
            System.out.println(toponym.getName()+" "+ toponym.getCountryName());
        }
    }
}