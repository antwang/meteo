package edu.xiaoleiwang.mymeteo;

import android.content.Context;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import edu.xiaoleiwang.mymeteo.remote.MeteoDataHelper;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context cxt = InstrumentationRegistry.getTargetContext();

        assertEquals("edu.xiaoleiwang.mymeteo", cxt.getPackageName());
        MeteoDataHelper helper = MeteoDataHelper.getInstance();
        Locale current;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            current = cxt.getResources().getConfiguration().getLocales().get(0);
        } else{
            //noinspection deprecation
            current = cxt.getResources().getConfiguration().locale;
        }
        Log.i("Test",current.getCountry());
        //assertEquals("fr",current.getCountry());
        //helper.getCurrentPositionMeteo(cxt,"metric");
        String res = helper.getForecastMeteoByCity("1792947","metric",current.getCountry());
        Log.i("weather",res.toString());
    }
    @Test
    public void testJsonLoader(){
        Context cxt = InstrumentationRegistry.getTargetContext();
        InputStream inputStream = cxt.getResources().openRawResource(R.raw.citys);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
            String line;
            List<City> cities = new ArrayList<>();
            long start = System.currentTimeMillis();

            while( (line = reader.readLine())!=null){
                City city = new Gson().fromJson(line,City.class);
                cities.add(city);
            }
            reader.close();
            long timeMillis = System.currentTimeMillis() - start;
            for(City city:cities){
                Log.i("jsonLoader",city.getCityName());
            }
            Log.i("loadcost",String.valueOf(timeMillis/1000));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static class City{
        @SerializedName("_id")
        private String id;
        @SerializedName("name")
        private String cityName;
        private String country;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }
}
